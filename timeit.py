#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# =============================================================================
# The MIT License
# 
# Copyright (c) 2020 LaSHarT-AI, (❤ LaSHarT ❤#4564)
# Server Support discord:  NaN
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
# =============================================================================
"""
TimeIt est une implementation afin de calculer un temps bien défini entre chaque tache !


Example d'utilisation:

    >>> taches = range(0, 50)
    >>> list_object = []

    >>> with TimeIt() as overall_timer:
    >>>     for tache in taches:

    >>>         with TimeIt() as timer:
    >>>             list_object.append(tache)

    >>>         print(f"tache n°{tache} effectuer en {timer.time_taken * 1000:,.2g}ms")
    >>> print(f"Toutes les taches ont était effectuer en {overall_timer.time_taken * 1000:,.2f}ms")
    
"""
import contextlib
import time

class TimeIt(contextlib.AbstractContextManager):
    def __init__(self):
        self.start = float("nan")
        self.end = float("nan")

    def __enter__(self):
        self.start = time.perf_counter()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.end = time.perf_counter()

    @property
    def time_taken(self):
        return self.end - self.start
